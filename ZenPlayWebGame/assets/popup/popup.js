﻿// JavaScript Document
$(document).ready(function () {
    $(".blackscene").click(function () {
        $(this).hide();
        $(".popup").hide();
    });
});

function showPopup() { $(".blackscene").fadeIn('fast'); $(".popup").hide(); $("#popup-date").fadeIn(); }
function showLogin() { $(".blackscene").fadeIn('fast'); $(".popup").hide(); $("#popup-log").fadeIn(); }
function showReg() { $(".blackscene").fadeIn('fast'); $(".popup").hide(); $("#popup-reg").fadeIn(); }
function closePopup() { $(".blackscene").hide(); $(".popup").hide(); }