﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ConnectGraph_Authorization : System.Web.UI.Page
{
    private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ServerProcess.GetAccessToken();
            log.Info("Access_token:" + AccountSession.AccessToken);

            var redirectUri = Request.Params["ur"] ?? string.Empty;
            if (string.IsNullOrEmpty(redirectUri))
                redirectUri = ServerProcess.GetSuccessUrl();

            // Redirect sang trang success
          // Response.Redirect(redirectUri);
        }
        catch (Exception ex)
        {
            log.Error(ex);
        }
    }
    
}