﻿using AdminCMS.Common;
using AdminCMS.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ZenPlayWebGame.Models;

namespace ZenPlayWebGame.Controllers
{
    public enum CategoryId
    {
        TIN_TUC = 1,
        SU_KIEN,
        HUONG_DAN,
    }
    //[Authorize]
    public class HomeController : Controller
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static string HOST = System.Configuration.ConfigurationManager.AppSettings["host"];

        public ActionResult Index()
        {
            if(Utils.fBrowserIsMobile())
            {
                return Redirect("/c/tin-tuc");
            }
            else
            {
                ViewBag.title3 = "ZenPlay68";
                return View();
            }          
        }
        public string Test()
        {
            return "test";
        }


        // id = 1, 2, 3
        public ActionResult News(string status = "tin-tuc")
        {
            int categoryId = 1;
            
            if (status == "tin-tuc")
            {
                categoryId = 1;
                ViewBag.title3 = "Tin tức";
            }
            if (status == "su-kien")
            {
                categoryId = 2;
                ViewBag.title3 = "Sự kiện";
            }
            if (status == "huong-dan")
            {
                categoryId = 3;
                ViewBag.title3 = "Hướng dẫn";
            }

            ZenPlayContextWeb db = new ZenPlayContextWeb();
            var hotNewsSmal = new List<WebNew>();
            var smallNews_1 = new List<WebNew>();
            var smallNews_2 = new List<WebNew>();
            var smallNews_3 = new List<WebNew>();

            var hotBigNew = new WebNew();
            var temp = new List<WebNew>();

            temp = db.WebNews.Where(wn => wn.Status == 1).ToList();
            temp = temp.Where(wn=>(wn.DatePublic - DateTime.Now).TotalSeconds <= 0).ToList();
            var temp_2 = temp.Where(h => h.HotNew == 1 ).OrderByDescending(h => h.DatePublic);
            hotBigNew = temp_2 != null ? temp_2.FirstOrDefault() : hotBigNew;
            hotNewsSmal = temp_2 != null ? temp_2.Skip(1).Take(2).ToList() : hotNewsSmal;

            smallNews_1 = temp.Where(h => h.HotNew == 0 && h.CategoryId == 1).OrderByDescending(h => h.DatePublic).ToList();
            smallNews_2 = temp.Where(h => h.HotNew == 0 && h.CategoryId == 2).OrderByDescending(h => h.DatePublic).ToList();
            smallNews_3 = temp.Where(h => h.HotNew == 0 && h.CategoryId == 3).OrderByDescending(h => h.DatePublic).ToList();


            ViewData["HotNewsSmall"] = hotNewsSmal;
            ViewData["SmallNews_1"] = smallNews_1;
            ViewData["SmallNews_2"] = smallNews_2;
            ViewData["SmallNews_3"] = smallNews_3;

            ViewData["HotBigNew"] = hotBigNew;
            ViewBag.message = "count:"+ temp_2.Count() +"-"+hotNewsSmal.Count;
            ViewBag.Host = HOST + "/d/";
            ViewBag.index = categoryId;


            ViewBag.android = RedisDB.GetLinkAndroid();
            ViewBag.ios = RedisDB.GetLinkIOS();

            return View();
        }

        public ActionResult Detail(string alias)
        {
            ZenPlayContextWeb db = new ZenPlayContextWeb();
            var webNew = db.WebNews.FirstOrDefault(wn => wn.Alias == alias);

            List<WebNew> firstNews = new List<WebNew>();
            var temp = new List<WebNew>();
            temp = db.WebNews.Where(wn => wn.Status == 1 && wn.CategoryId == webNew.CategoryId).ToList();
            temp = temp.Where(wn => (wn.DatePublic - DateTime.Now).TotalSeconds <= 0).OrderByDescending(wn => wn.DatePublic).ToList();

            for (int k = 0; k < temp.Count; k++)
            {
                if(firstNews.Any(fn=>fn.Title == temp[k].Title))
                {
                    continue;
                }
                firstNews.Add(temp[k]);
                if (firstNews.Count >= 4)
                    break;

            }
         
            ViewData["FirstNews"] = firstNews == null ? new List<WebNew>() : firstNews.ToList();
            ViewBag.Host = HOST + "/d/";

            ViewBag.android = RedisDB.GetLinkAndroid();
            ViewBag.ios = RedisDB.GetLinkIOS();

            if (webNew != null)
            {
                var back = "";
                if (webNew.CategoryId == 1)
                {
                    ViewBag.category = "Tin tức";
                    back = "tin-tuc";
                }
                else if (webNew.CategoryId == 2)
                {
                    ViewBag.category = "Sự kiện";
                    back = "su-kien";
                }
                else if (webNew.CategoryId == 3)
                {
                    ViewBag.category = "Hướng dẫn";
                    back = "huong-dan";
                }
                ViewBag.category_host = "/c/" + back;

                ViewBag.title2 = webNew.Title;
                ViewBag.href = HOST + "/d/" + alias;
                ViewBag.Detail = webNew.ShortDes;
                ViewBag.data = webNew.Content;
                ViewBag.datetime = webNew.DatePublic.ToString("yyyy-MM-dd HH:mm:ss");

                ViewBag.title3 = webNew.Title;
            }
            else
            {
                ViewBag.title2 = "Đây là title test:" + alias;
                ViewBag.href = "http://google.com.vn";
                ViewBag.Detail = "hola hola";
                ViewBag.data = "Để sự kiện có thể phù hợp hơn với mọi người chơi, ZenPlay xin phép được thay đổi 1 chút trong event đua TOP mini game hàng tuần.\nBắt đầu từ 30 / 10 / 2017 event đua TOP mini game diễn ra hằng tuần chính thức sẽ bỏ đua TOP mini game bằng GOLD, từ giờ đua TOP mini game sẽ chỉ được tính bằng ZEN và giải thưởng sẽ thuộc về người nào xếp hạng 1-2-3 các mini game chơi bằng ZEN.";
                ViewBag.datetime = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

            }
            return View();
        }

        public ActionResult DownloadGameAndroid()
        {
            var dir = Server.MapPath("/App_Data");
            var path = Path.Combine(dir, "zenplay.apk");
            return base.File(path, "application/octet-stream", "zenplay.apk");
        }
        public ActionResult DownloadGameIOS()
        {
            var dir = Server.MapPath("/App_Data");
            var path = Path.Combine(dir, "zenplay.apk");
            return base.File(path, "application/octet-stream", "zenplay.apk");
        }
    }
}
