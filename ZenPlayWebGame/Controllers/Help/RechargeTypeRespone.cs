﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

/// <summary>
/// Summary description for RechargeTypeRespone
/// </summary>

    public class RechargeTypeRespone
    {
        [JsonProperty(PropertyName = "name"), DataMember(Name = "name")]
        public string Name { get; set; }

        [JsonProperty(PropertyName = "url"), DataMember(Name = "url")]
        public string Url { get; set; }

        [JsonProperty(PropertyName = "type"), DataMember(Name = "type")]
        public string Type { get; set; }

        [JsonProperty(PropertyName = "icon"), DataMember(Name = "icon")]
        public string Icon { get; set; }
    }
